package com.anton.websearch.constant;

public final class URLConstant {

    private URLConstant() {

    }

    public static final String SEARCH_GOOGLE_URL = "https://www.google.com/search?q=";

    public static final int STATUS_SUCCESS = 200;

    public static final String URL_CSS_QUERY = "h3.r  a";

}
