package com.anton.websearch.action.impl;

import com.anton.websearch.action.Action;
import com.anton.websearch.service.SearchService;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SearchActionImpl implements Action {


    @Override
    public void performAction() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the searching value");
        String searchValue = scanner.nextLine();
        int amountResults = inputResultCount(scanner);
        SearchService.performSearch(searchValue, amountResults);

    }

    /**
     * @param scanner scanner;
     * @return amount of results to search.
     */
    private int inputResultCount(Scanner scanner) {
        int amountResults = 0;
        boolean error = true;
        while (error) {
            System.out.println("Enter amount of results > 0 && <= 50 ");
            try {
                amountResults = scanner.nextInt();
                if (amountResults < 0 || amountResults == 0 || amountResults > 50) {
                    throw new InputMismatchException();
                }
                error = false;

            } catch (InputMismatchException e) {
                System.out.println("Wrong value!");
                scanner.nextLine();
                error = true;
            }
        }
        return amountResults;
    }
}
