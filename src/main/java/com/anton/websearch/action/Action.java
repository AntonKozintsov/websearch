package com.anton.websearch.action;

public interface Action {

    void performAction();
}
