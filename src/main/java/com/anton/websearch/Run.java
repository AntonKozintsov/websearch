package com.anton.websearch;

import com.anton.websearch.action.Action;
import com.anton.websearch.constant.ActionConstant;
import com.anton.websearch.util.InitData;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Run {

    public static void main(String[] args) {
        Map<String, Action> actionMap = new HashMap<>();
        InitData.initMap(actionMap);
        Action action;

        System.out.println("Enter the action you want to perform [exit]" + actionMap.keySet());
        Scanner scanner = new Scanner(System.in);
        String key = scanner.nextLine();
        while (!key.equals(ActionConstant.EXIT)) {
            if (actionMap.containsKey(key)) {
                action = actionMap.get(key);
                action.performAction();
            } else {
                System.out.println("no such key -[" + key + "] " + "Enter one of provided! [search, exit]");
            }
            key = scanner.nextLine();
        }
        scanner.close();
    }

}
