package com.anton.websearch.service;

import com.anton.websearch.constant.URLConstant;
import com.anton.websearch.entity.SearchResult;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class SearchService {

    public static void performSearch(String searchValue, int amountResults) {
        Set<SearchResult> searchResults = new HashSet<>();
        String searchURL = URLConstant.SEARCH_GOOGLE_URL + searchValue + "&num=" + amountResults;
        Connection.Response response;
        Document document = null;
        try {
            //User agent to prevent search block from google.
            response = Jsoup.connect(searchURL).userAgent("Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)").execute();
            if (response.statusCode() == URLConstant.STATUS_SUCCESS) {
                document = response.parse();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Elements results;
        if (document != null) {
            results = document.select(URLConstant.URL_CSS_QUERY);
            Element oneResult = document.selectFirst(URLConstant.URL_CSS_QUERY);
            SearchResult firstResult = new SearchResult(oneResult.text(), parseUrl(oneResult.attr("href")));
            System.out.println("The first result is: " + firstResult);
            for (Element result : results) {
                String url = parseUrl(result.attr("href"));
                String title = result.text();
                searchResults.add(new SearchResult(title, url));
            }
            System.out.println(searchResults);
        } else {
            System.out.println("Server returned null, try later!");
        }
    }

    private static String parseUrl(String url) {
        return url.substring(url.indexOf("=") + 1, url.indexOf("&"));
    }
}
