package com.anton.websearch.util;

import com.anton.websearch.action.Action;
import com.anton.websearch.action.impl.SearchActionImpl;

import java.util.Map;

public class InitData {


    public static void initMap(Map<String, Action> actionMap) {
        actionMap.put("search", new SearchActionImpl());
    }
}
